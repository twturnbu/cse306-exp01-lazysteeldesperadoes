#include "defs.h"
#include "CUnit.h"
#include "Basic.h"
#include <stdio.h>
#include <stdbool.h>
#include "Expr.h"


void addTest(int, int, int);

void test_add(void) {addTest( 5, 6, 11); }

void addTest( int num1, int num2, int expected){
 
  struct Expr * expOutput = Number(expected);  
  struct  Expr * ex1 = Number(num1);
  struct  Expr * ex2 = Number(num2);
  struct Expr *actual = add(ex1, ex2);

 // int expectedInt= valueOf(ex1);
   printf("INPUT: \"%d\", \"%d\" , EXPECTED: \"%d\", ACTUAL: \"%d\"...", valueOf(ex1), valueOf(ex2), valueOf(expOutput), valueOf(actual));
 
   CU_ASSERT_EQUAL(valueOf(actual),valueOf(expOutput));
 
}


/* The main function for setting up and runnind the tests.
 * Returns CUE_SUCCESS of succesful running, another 
 * CUnit error code on failure.
 */
int main(){
  CU_pSuite pSuite = NULL;

  /* initialie the cunit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())  { return CU_get_error(); }

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite1",NULL,NULL);

  if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
      }
  // Add tests to suite
  if ( 
	(NULL == CU_add_test(pSuite, "test_add", test_add))
  )

  {
	CU_cleanup_registry();
	return CU_get_error();
  }

  /*run all tests using CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  //	CU_basic_show_failures(CU_get_failure_list());
  CU_cleanup_registry();
  return CU_get_error();
}
